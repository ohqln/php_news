<?php
require_once 'model/App.php';
App::init();

$db = new Database();

$sr = new CategoryRepository($db);

$categories = $sr->getCategories();
$i = 1;


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>news</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styler.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
<?php require "header.php" ?>
<div class="w-50 p-3">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Kategorie</th>
            <th scope="col">Operace</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categories  as $a): ?>
            <tr>
                <th scope="row"><?= $i++ ?></th>
                <td><?= $a['name']?></td>

                <td>
                    <a href="articles_by_category.php?category=<?= $a['id'] ?>" class="btn btn-primary" href="#" role="button">Články</a>
                    <!--<a href="author_edit.php?id=<?= $a['id'] ?>" class="btn btn-primary" href="#" role="button">Upravit</a>
                    <a href="category_delete.php?id=<?= $a['id'] ?>" class="btn btn-primary" href="#" role="button">Smazat</a>-->
                </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>


</body>
</html>
