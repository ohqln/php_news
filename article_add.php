<?php

//require_once 'model/Database.php';
//require_once 'model/AuthorRepository.php';
//require_once 'model/ArticleRepository.php';

require_once 'model/App.php';
App::init();



$db = new Database();

$ar = new ArticleRepository($db);

$author = new AuthorRepository($db);

$authors = $author->getAuthors();

$cr = new CategoryRepository($db);

$categories = $cr->getCategories();



if (isset($_POST['title'], $_POST['text'], $_POST['idAuthor'], $_POST['idCategory'])) {

    $ar->AddArticle($_POST['title'], $_POST['text'],
        $_POST['idAuthor'], $_POST['idCategory']);


    header('Location: index.php');
    die();

    // $stmt->rowCount();
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Přidat studenta</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styler.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<?php require "header.php" ?>

<div class="w-50 p-3">
    <h2>Přidat článek</h2>
    <form action="" method="post">

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Titulek</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="title"></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea2">Text článku</label>
            <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" name="text"></textarea>
        </div>


        <div class="form-group">
            <label for="exampleFormControlSelect3">Autor</label>
            <select class="form-control" id="exampleFormControlSelect3" name="idAuthor">
                <?php foreach ($authors as $a): ?>
                    <option value="<?= $a['id'] ?>"><?= $a['name'] . " " . $a['surname'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect4">Kategorie</label>
            <select class="form-control" id="exampleFormControlSelect4" name="idCategory">
                <?php foreach ($categories as $c): ?>
                    <option value="<?= $c['id'] ?>"><?= $c['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div>
            <button class="btn btn-primary btn-lg" style="margin-top: 30px;">Přidat článek</button>
        </div>

    </form>
</div>

</body>
</html>
