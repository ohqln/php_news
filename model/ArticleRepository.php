<?php

require_once 'Database.php';

class ArticleRepository
{
    private $db;

    /**
     * ArticleRepositoory constructor.
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }
    public function getArticles()
    {
        $sql = 'SELECT category.name AS categoryName, author.name as authorName,
                author.surname as authorSurname, article.* FROM article
                INNER JOIN author ON article.idAuthor = author.id
                INNER JOIN category ON article.idCategory = category.id
                order by article.date desc
                LIMIT 3';

        return $this->db->selectAll($sql);
    }
    public function getArticle($id)
    {
        $sql = 'SELECT category.name AS categoryName, author.name as authorName,
                author.surname as authorSurname, article.* FROM article
                INNER JOIN author ON article.idAuthor = author.id
                INNER JOIN category ON article.idCategory = category.id
                where article.id = :id';

        return $this->db->selectOne($sql, [
            ':id' => $id
        ]);
    }

    public function getArticlesByCategory($category)
    {
        $sql = 'SELECT category.name AS categoryName, author.name as authorName,
                author.surname as authorSurname, article.* FROM article
                INNER JOIN author ON article.idAuthor = author.id
                INNER JOIN category ON article.idCategory = category.id
                where category.id = :category
                order by article.date desc';

        return $this->db->selectAll($sql, [
            ':category' => $category
        ]);
    }
    public function getArticlesByAuthor($author)
    {
        $sql = 'SELECT category.name AS categoryName, author.name as authorName,
                author.surname as authorSurname, article.* FROM article
                INNER JOIN author ON article.idAuthor = author.id
                INNER JOIN category ON article.idCategory = category.id
                where author.id = :author
                order by article.date desc';

        return $this->db->selectAll($sql, [
            ':author' => $author
        ]);
    }
    public function AddArticle($title, $text, $idAuthor, $idCategory)
    {
        $sql = 'INSERT INTO article 
                SET title = :title, text = :text, idAuthor = :idAuthor, idCategory = :idCategory';
        $data = [
            ':title' => $title,
            ':text' => $text,
            ':idAuthor' => $idAuthor,
            ':idCategory' => $idCategory,
        ];

        $this->db->insert($sql, $data);
    }

}