<?php


class App
{

    public static function init()
    {
        spl_autoload_register([
            __CLASS__,
            'load'
        ]);
    }

    // App::load('Database')
    public static function load($className)
    {
        require_once "$className.php";
    }

}