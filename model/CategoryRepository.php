<?php

require_once 'Database.php';

class CategoryRepository
{
    private $db;

    /**
     * StudentRepository constructor.
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getCategories()
    {
        $sql = 'SELECT * FROM category';

        return $this->db->selectAll($sql);
    }
    public function deleteCategory($id)
    {
        $sql = 'DELETE FROM category WHERE id = :id';
        $this->db->update($sql, [
            ':id' => $id
        ]);
    }

}