<?php

require_once 'Database.php';

class AuthorRepository
{
    private $db;

    /**
     * StudentRepository constructor.
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getAuthors()
    {
        $sql = 'SELECT * FROM author';

        return $this->db->selectAll($sql);
    }


    public function getAuthor($id)
    {
        $sql = 'SELECT * FROM author WHERE id = :id';

        return $this->db->selectOne($sql, [
            ':id' => $id
        ]);
    }

    public function addAuthor($name, $surname, $email)
    {
        $sql = 'INSERT INTO author 
                SET name = :name, surname = :surname, email = :email';
        $data = [
            ':name' => $name,
            ':surname' => $surname,
            ':email' => $email,
        ];

        $this->db->insert($sql, $data);
    }

    public function updateAuthor($name, $surname, $email, $id)
    {
        $sql = 'UPDATE author
                SET name = :name, surname = :surname, email = :email
                WHERE id = :id';
        $data = [
            ':name' => $name,
            ':surname' => $surname,
            ':email' => $email,
            ':id' => $id,
        ];

        $this->db->update($sql, $data);
    }

    public function deleteAuthor($id)
    {
        $sql = 'DELETE FROM author WHERE id = :id';
        $this->db->update($sql, [
            ':id' => $id
        ]);
    }
}