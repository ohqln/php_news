<?php

class Database
{
    const HOST = 'localhost';
    const DBNAME = 'news';
    const USER = 'root';
    const PASSWORD = '';

    private $conn;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        // $cs = "mysql:host={self::HOST};dbname={self::DBNAME}";

        $this->conn = new PDO("mysql:host=" . self::HOST . ";dbname=" . self::DBNAME,
            self::USER, self::PASSWORD,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);
        $this->conn->query('SET NAMES utf8');

    }

    public function selectAll($query, $params = []) {
        $stmt = $this->execute($query, $params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectOne($query, $params) {
        $stmt = $this->execute($query, $params);
        return $stmt->fetch();
    }

    public function insert($query, $params) {
        $this->execute($query, $params);
        return $this->conn->lastInsertId();
    }

    public function update($query, $params) {
        $stmt = $this->execute($query, $params);
        return $stmt->rowCount();
    }

    protected function execute($query, $params = []) {
        $stmt = $this->conn->prepare($query);
        $stmt->execute($params);

        return $stmt;
    }
}