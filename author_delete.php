<?php

require_once 'model/Database.php';
require_once 'model/AuthorRepository.php';

$db = new Database();

$sr = new AuthorRepository($db);
$authors = $sr->deleteAuthor($_GET['id']);


header('Location: authors.php');
