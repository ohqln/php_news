<?php

require_once 'model/Database.php';
require_once 'model/AuthorRepository.php';

$db = new Database();

$sr = new AuthorRepository($db);

//$author = $sr->getAuthor($_GET['id']);




if (isset($_POST['name'], $_POST['surname'], $_POST['email'])) {

    $sr->addAuthor($_POST['name'], $_POST['surname'],
        $_POST['email']);


    header('Location: authors.php');
    die();

    // $stmt->rowCount();
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Přidat studenta</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styler.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<?php require "header.php" ?>

<div class="w-50 p-3">
    <h2>Upravit autora</h2>
    <form action="" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Jméno</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   name="name">
        </div>

            <div class="form-group">
                <label for="exampleInputEmail2">Příjmení</label>
                <input type="text" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp"
                       name="surname">
            </div>
                <div class="form-group">
                    <label for="exampleInputEmail3">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp"
                           name="email">
                </div>



                <div>
                    <button class="btn btn-primary">Submit</button>
                </div>

    </form>
</div>

</body>
</html>
