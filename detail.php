<?php
require_once 'model/App.php';
App::init();

$db = new Database();

$sr = new ArticleRepository($db);

$article = $sr->getArticle($_GET['id']);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>news</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="styler.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <style>
        .title{
            color: #007bff;
            font-size: 200%;
            margin-top: 50px;
        }
        .jmeno{
            color:#007bff;
            margin-top: 10px;
            padding-top: 40px;
        }
        .jmeno small{
            color:grey;
        }

    </style>
</head>
<body>
<?php require "header.php" ?>
<div class="w-75 p-3" >
    <article class="article">


            <h1 class="title">
                <?= $article['title']?>
            </h1>
            <p class="jmeno"><small><?= $article['date'] . " "?></small> <?=$article['authorName'] . " " . $article['authorSurname']?></p>

            <p><?= $article['text']?></p>

    </article>

</div>


</body>
</html>